#pragma once
#ifndef __AUX_HPP__
#define __AUX_HPP__

#include <any>
#include <exception>
#include <memory>
#include <optional>
#include <vector>

class Debug {
  public:
  void debug(int indent) {};
};

class OptMultisetException : public std::exception {
  virtual const char *what() const throw() {
    return "Trying to set more than once";
  }
};

// If val is empty, set it (with move semantics). If it was already set, raise
// an error.
template <typename T> void set_once(std::optional<T> &dest, T new_val) {
  if (dest) {
    throw OptMultisetException();
  } else {
    dest = std::move(new_val);
  }
}

// If types match:
//   If val is empty, set it and return true. If it was already set, raise an
//   error.
// If types don't match, do nothing and return false.
template <typename T>
bool set_once_if_match(std::optional<T> &dest, std::any src) {
  if (src.type() == typeid(T)) {
    set_once(dest, std::any_cast<T>(src));
    return true;
  }
  return false;
};

// std::vector pop with move semantics
template <typename T> T pop(std::vector<T> &vec) {
  T rv = std::move(vec.back());
  vec.pop_back();
  return rv;
}

#endif