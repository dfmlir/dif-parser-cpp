#include "dif-parser.hpp"

using namespace DIF;

void Edge::debug(int indent) {
  for (int i = 0; i < indent; i++)
    std::cout << " ";
  std::cout << name << ": " << node_a->name << " -> " << node_b->name << "\n";
}

void Topology::debug(int indent) {
  for (auto &edge : edges)
    edge.debug(indent);
}

void Graph::debug(int indent) {
  for (int i = 0; i < indent; i++)
    std::cout << " ";
  std::cout << name << ": " << moc << '\n';
  topology.debug(indent + 2);
}
