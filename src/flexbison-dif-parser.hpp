#include "dif-parser.hpp"
#include <algorithm>
#include <any>
#include <cstdio>
#include <typeinfo>
#include <variant>

extern FILE *yyin;
extern int yylex();
extern char *yytext;
extern const char **token_table;

class FlexBisonDIFParser : public DIF::DIFParser {
  std::vector<std::any> stack;
  std::map<std::string, DIF::Node> *current_nodes;

public:
  void fold_Graph();
  void fold_Name();
  void fold_Nodes();
  void fold_Edge();
  void fold_Topology();
  void push_sentinel();

  template <typename T> void fold_vector() {

    std::vector<T> elements;

    // Expected pattern: ... {SENTINEL} {element}*
    //                                            ^
    std::any item;

    while (!stack.empty()) {
      item = pop(stack);
      try {
        auto element = std::any_cast<T>(item);
        elements.push_back(element);
        continue;
      } catch (std::bad_any_cast &e) {
        break;
      }
    }

    // Expected pattern: ... {SENTINEL} {element}*
    //                                 ^

    try {
      auto x = item.type().name();
      auto element = std::any_cast<const int>(item);
    } catch (std::bad_any_cast &e) {
      throw DIF::Error("Bad stack");
    }

    std::reverse(elements.begin(), elements.end());
    stack.push_back(std::any(elements));
  }

  std::vector<DIF::Graph> parse_dif_file(FILE *file) override;
};

extern FlexBisonDIFParser *parser;