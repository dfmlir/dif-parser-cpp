#include "flexbison-dif-parser.hpp"
#include "aux.hpp"
#include "parser.hpp"

const int SENTINEL = -1;

void FlexBisonDIFParser::fold_Graph() {

  std::string name;
  std::string moc;
  std::optional<DIF::Topology> topology;

  // Expected pattern: ... {moc} {name} {block}*
  //                                            ^

  while (true) {
    auto item = pop(stack);
    if (set_once_if_match(topology, item)) {
      continue;
    };

    // ... other blocks

    // Expected pattern: ... {moc} {name} {block}*
    //                                   ^
    name = std::move(std::any_cast<std::string>(item));
    item = pop(stack);
    moc = std::move(std::any_cast<std::string>(item));
    break;
  }

  auto item =
      std::any(new DIF::Graph(*topology, std::move(name), std::move(moc)));

  stack.push_back(item);
}

void FlexBisonDIFParser::push_sentinel() {
  stack.push_back(std::any(SENTINEL));
}

void FlexBisonDIFParser::fold_Name() { stack.push_back(std::string(yytext)); }

void FlexBisonDIFParser::fold_Nodes() {
  if (current_nodes == NULL) {
    current_nodes = new std::map<std::string, DIF::Node>();
  }

  // Expected pattern: ... {SENTINEL} {element}*
  //                                            ^
  std::any item;

  while (!stack.empty()) {
    item = pop(stack);
    if (item.type() == typeid(std::string)) {
      auto name = std::any_cast<std::string>(item);
      if (current_nodes->find(name) != current_nodes->end()) {
        // do nothing
      } else {
        current_nodes->emplace(name, name);
      }
    } else {
      break;
    }
  }

  // Expected pattern: ... {SENTINEL} {element}*
  //                                 ^

  if (item.type() != typeid(SENTINEL)) {
    throw DIF::Error("Bad stack");
  }
  stack.push_back(std::any(current_nodes));
}

void FlexBisonDIFParser::fold_Edge() {

  std::vector<std::string> names;

  for (int i = 0; i < 3; i++) {
    auto item = pop(stack);
    if (item.type() == typeid(std::string)) {
      names.push_back(std::any_cast<std::string>(item));
    } else {
      throw DIF::Error("Bad stack: expected name");
    }
  }

  std::reverse(names.begin(), names.end());
  stack.push_back(std::any(names));
}

void FlexBisonDIFParser::fold_Topology() {
  std::vector<std::vector<std::string>> edges;
  std::vector<DIF::Node> nodes;

  for (int i = 0; i < 2; i++) {
    auto item = pop(stack);
    if (item.type() == typeid(edges)) {
      edges = std::move(std::any_cast<decltype(edges)>(item));
    } else if (item.type() == typeid(nodes)) {
      nodes = std::move(std::any_cast<decltype(nodes)>(item));
    } else {
      throw DIF::Error("Bad stack: expected list of names or list of nodes");
    }
  }
}

std::vector<DIF::Graph> FlexBisonDIFParser::parse_dif_file(FILE *file) {
  yyin = file;
  parser = this;
  yyparse();
  std::vector<DIF::Graph> rv;
  std::transform(stack.begin(), stack.end(), std::back_inserter(rv),
                 [](std::any item) { return std::any_cast<DIF::Graph>(item); });
  return rv;
}
